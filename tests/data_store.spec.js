/* globals jest, describe, it, expect */

import * as firebaseApp from "firebase/app";
import * as firebaseFirestore from "firebase/firestore";
import firebaseConfig from "../src/firebase.secret";
import "../src/data_store";

const app = "fooApp";
const db = "fooDB";

jest.mock("firebase/app", () => ({
  initializeApp: jest.fn().mockImplementation(() => app),
}));
jest.mock("firebase/firestore", () => ({
  getFirestore: jest.fn().mockImplementation(() => db),
  collection: jest.fn(),
}));

describe("Data storage", () => {
  it("initialises the Firebase app with the correct config", () => {
    expect(firebaseApp.initializeApp).toHaveBeenCalledWith(firebaseConfig);
  });
  it("correctly connects to the database", () => {
    expect(firebaseFirestore.getFirestore).toHaveBeenCalledWith(app);
  });
  it("fetches correct collection in the db", () => {
    expect(firebaseFirestore.collection).toHaveBeenCalledWith(db, "times");
  });
});
