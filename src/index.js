import "tw-elements";
import { setQueryListener, getEntries, addEntry } from "./firebase_integration";

function formatTime(minNum) {
  let hours = Math.floor(minNum / 60);
  let minutes = Math.floor(minNum - hours * 60);

  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }

  return `${hours}h:${minutes}min`;
}

function getTotalTime(durations) {
  const container = document.getElementById("total-duration");
  container.innerHTML = formatTime(
    durations.reduce(
      (previousValue, currentValue) => previousValue + currentValue,
      0
    )
  );
}

async function renderRows(snapshot) {
  const timesList = snapshot.docs.map((doc) => doc.data());
  const durations = [];

  const template = document.getElementById("entry-row");
  const frag = document.createDocumentFragment();
  let clone;
  timesList.forEach((entry) => {
    clone = template.content.cloneNode(true);
    const description = clone.querySelector(
      '[data-template-hook="description"]'
    );
    const startTransformed = clone.querySelector(
      '[data-template-hook="startTransformed"]'
    );
    const endTransformed = clone.querySelector(
      '[data-template-hook="endTransformed"]'
    );
    const keywords = clone.querySelector('[data-template-hook="keywords"]');
    const duration = clone.querySelector('[data-template-hook="duration"]');

    const durationNum = (entry.end.toDate() - entry.start.toDate()) / 1000 / 60;

    description.textContent = entry.description;
    startTransformed.textContent = entry.start.toDate().toLocaleString();
    endTransformed.textContent = entry.end.toDate().toLocaleString();
    keywords.textContent = entry.keywords.join(", ");
    duration.textContent = formatTime(durationNum);

    durations.push(durationNum);
    frag.appendChild(clone);
  });

  const container = document.getElementById("content-rows");
  container.replaceChildren(frag);

  getTotalTime(durations);
}

async function renderEntries() {
  const timesSnapshot = await getEntries();
  renderRows(timesSnapshot);
}

async function postEntry(data) {
  try {
    const docRef = await addEntry(data);
    // eslint-disable-next-line no-console
    console.log("Document written with ID: ", docRef.id);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error("Error adding document: ", e);
  }
}

async function setupSubmitListener(e) {
  e.preventDefault();
  e.stopPropagation();
  const form = e.target;
  const inputs = Array.from(form.elements).filter(
    (el) => el.nodeName === "INPUT"
  );
  const data = {};
  inputs.forEach((input) => {
    switch (input.id) {
      case "start":
      case "end":
        data[input.id] = new Date(input.value);
        break;
      case "keywords":
        data[input.id] = input.value.split(", ");
        break;
      default:
        data[input.id] = input.value;
    }
  });
  await postEntry(data);
  form.reset();
}

function bootstrap() {
  renderEntries();

  setQueryListener(renderRows);

  const form = document.getElementById("add-new-time");
  form.addEventListener("submit", setupSubmitListener);
}

bootstrap();

export default bootstrap;
