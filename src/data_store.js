import { initializeApp } from "firebase/app";
import { getFirestore, collection } from "firebase/firestore";
import firebaseConfig from "./firebase.secret";

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
collection(db, "times");
