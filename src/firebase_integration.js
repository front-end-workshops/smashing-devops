// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  getDocs,
  addDoc,
  query,
  onSnapshot,
} from "firebase/firestore";
import firebaseConfig from "./firebase.secret";

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const getCollection = () => collection(db, "times");

export const addEntry = (data) => addDoc(getCollection(), data);
export const getEntries = () => getDocs(getCollection());
export const setQueryListener = (cb) => {
  const q = query(getCollection());
  onSnapshot(q, (querySnapshot) => {
    cb(querySnapshot);
  });
};
