/* eslint global-require: 0 */
module.exports = {
  content: [
    "./src/**/*.{html,pug,js}",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  theme: {
    extend: {},
  },
  plugins: [require("tw-elements/dist/plugin")],
};
